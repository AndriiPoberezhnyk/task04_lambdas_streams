package com.epam.training.controller;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AppMethods {
    private static Scanner scanner = new Scanner(System.in);
    public static String getTextFromUser(){
        Stream.Builder<String> streamText = Stream.builder();
        String line = " ";
        while (scanner.hasNextLine() && !(line = scanner.nextLine()).equals("")){
            streamText.accept(line);
        }
        Stream<String> text = streamText.build();
        StringBuilder textBuilder = new StringBuilder();
        text.forEach(w -> textBuilder.append(w + " "));
        return textBuilder.toString();
    }

    public static List<Character> toCharList(String text){
        List<Character> chars = text.chars()
                .mapToObj(c -> (char) c)
                .filter(c -> Character.isAlphabetic((int)c)
                        || c==' ')
                .collect(Collectors.toList());
        return chars;
    }

    public static List<String> toStringList(List<Character> characterList){
        StringBuilder checkedText = new StringBuilder();
        characterList.forEach(c -> checkedText.append(c));
        List<String> words =
                Arrays.asList(checkedText.toString().split(" "));
        return words;
    }

    public static int getNumberOfUniqueWords(List<String> list){
        return getSetOfUniqueWords(list).size();
    }

    private static Set<String> getSetOfUniqueWords(List<String> list){
        list = list.stream()
                .map(String::toLowerCase)
                .collect(Collectors.toList());
        return new HashSet<>(list);
    }

    public static Set<String> getSortedWords(List<String> list){
        Set<String> wordSet = getSetOfUniqueWords(list);
        Set<String> orderedWords = new TreeSet<>(wordSet);
        return orderedWords;
    }

    public static Map<String, Integer> getWordCount(List<String> list){
        Map<String, Integer> wordCount = new HashMap<>();
        list = list.stream()
                .map(String::toLowerCase)
                .collect(Collectors.toList());
        List<String> finalList = list;
        getSetOfUniqueWords(list).forEach(w -> {
            int numberOfRepeats = (int)
                    finalList.stream().filter(word -> word.equals(w)).count();
            wordCount.put(w, numberOfRepeats);
        });
        return wordCount;
    }

    public static Map<Character, Integer> getSymbolsCount(List<Character> list){
        Map<Character, Integer> symbolsCount = new HashMap<>();
        list = list.stream()
                .filter(Character::isLowerCase)
                .collect(Collectors.toList());
        List<Character> finalList = list;
        Set<Character> uniqueSymbols = new HashSet<>(finalList);
        uniqueSymbols = new TreeSet<>(uniqueSymbols);
        uniqueSymbols.forEach(w -> {
            int numberOfRepeats = (int)
                    finalList.stream().filter(c -> c == w).count();
            symbolsCount.put(w, numberOfRepeats);
        });
        return symbolsCount;
    }

}
