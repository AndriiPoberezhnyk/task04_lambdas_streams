package com.epam.training.controller;

import java.util.List;
import java.util.stream.Collectors;

public class IntegerListMethods {
    public static int getAverage(List<Integer> integerList) {
        return getSum(integerList) / integerList.size();
    }

    public static int getAverageByIntStream(List<Integer> integerList) {
        return (int)integerList.stream()
                .mapToInt(Integer::intValue)
                .average().getAsDouble();
    }

    public static int getSum(List<Integer> integerList) {
        return integerList.stream()
                .reduce(0, Integer::sum);
    }

    public static int getSumByIntStream(List<Integer> integerList) {
        return integerList.stream()
                .mapToInt(Integer::intValue)
                .sum();
    }

    public static int getMax(List<Integer> integerList) {
        return integerList.stream()
                .reduce(Integer.MIN_VALUE, Integer::max);
    }

    public static int getMaxByIntStream(List<Integer> integerList) {
        return integerList.stream()
                .mapToInt(Integer::intValue)
                .max().getAsInt();
    }

    public static int getMin(List<Integer> integerList) {
        int min = integerList.stream()
                .reduce(Integer.MAX_VALUE, Integer::min).intValue();
        return min;
    }

    public static int getMinByIntStream(List<Integer> integerList) {
        return integerList.stream()
                .mapToInt(Integer::intValue)
                .min().getAsInt();
    }

    public static long countBiggerThanAverage(List<Integer> integerList) {
        int avg = getAverage(integerList);
        return integerList.stream()
                .filter(v -> v > avg).count();
    }

    public static int countBiggerThanAverageBySize(List<Integer> integerList) {
        int avg = getAverageByIntStream(integerList);
        integerList = integerList.stream()
                .filter(v -> v > avg).collect(Collectors.toList());
        return integerList.size();
    }

    public static String getListInfo(List<Integer> integerList){
        return "Max = " + getMax(integerList)
                + ", min = " + getMin(integerList)
                + ", avg = " + getAverage(integerList)
                + ", sum = " + getSum(integerList);
    }

    public static String getListInfoByIntStream(List<Integer> integerList){
        return "Max = " + getMaxByIntStream(integerList)
                + ", min = " + getMinByIntStream(integerList)
                + ", avg = " + getAverageByIntStream(integerList)
                + ", sum = " + getSumByIntStream(integerList);
    }


}
