package com.epam.training.model.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Light {
    private static final Logger logger =
            LogManager.getLogger(Light.class.getName());

    public Light() {
    }

    public void turnMax() {
        logger.info("The light brightness is maximum");
    }

    public void turnAvg() {
        logger.info("The light brightness is average");
    }

    public void turnMin() {
        logger.info("The light brightness is minimum");
    }
}