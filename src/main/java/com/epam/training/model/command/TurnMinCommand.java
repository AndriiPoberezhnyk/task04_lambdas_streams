package com.epam.training.model.command;

import com.epam.training.functionals.Command;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TurnMinCommand implements Command {
    private static final Logger logger =
            LogManager.getLogger(Light.class.getName());

    private Light theLight;

    @Override
    public void execute() {
        logger.info("The light brightness is minimum");
    }
}
