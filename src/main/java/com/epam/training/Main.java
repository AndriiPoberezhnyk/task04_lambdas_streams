package com.epam.training;

import com.epam.training.view.View;

public class Main {
    public static void main(String[] args) {
        new View().run();
    }
}
