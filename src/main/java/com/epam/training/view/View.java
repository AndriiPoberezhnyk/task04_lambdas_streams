package com.epam.training.view;

import com.epam.training.Main;
import com.epam.training.functionals.Command;
import com.epam.training.model.command.*;
import com.epam.training.controller.AppMethods;
import com.epam.training.functionals.TripleFunction;
import com.epam.training.listGenerators.IntegerListGenerator;
import com.epam.training.controller.IntegerListMethods;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;
import java.util.function.Consumer;

public class View {
    static final Logger logger = LogManager.getLogger(Main.class.getName());
    private List<Integer> generatedList;
    private List<Character> textInChars;
    private List<String> textInStrings;
    private List<String> menu;
    private Map<String, Runnable> methodsMenu;
    private Scanner scanner;


    public View() {
        scanner = new Scanner(System.in);
        menu = new LinkedList<>();
        methodsMenu = new LinkedHashMap<>();
        generatedList = new ArrayList<>();
    }

    public void run() {
        String keyMenu;
        do {
            generateMainMenu();
            System.out.println("Please, select menu point.");
            keyMenu = scanner.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).run();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String option : menu) {
            System.out.println(option);
        }
        System.out.print(">>> ");
    }

    private void generateMainMenu() {
        menu.clear();
        menu.add("1 - Show lambda task result");
        menu.add("2 - Show command task menu");
        menu.add("3 - Show streams task menu");
        menu.add("4 - Show application task menu");
        menu.add("Q - Exit");

        methodsMenu.clear();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
        outputMenu();
    }

    private void pressButton1() {
        TripleFunction<Integer> maxValue =
                ((a, b, c) -> a > b ? Math.max(a, c) : Math.max(b, c));
        TripleFunction<Integer> avgValue =
                ((a, b, c) -> (a + b + c) / 3);
        int max = maxValue.apply(10, 15, 5);
        int avg = avgValue.apply(10, 15, 5);
        logger.info(max);
        logger.info(avg);
    }

    private void pressButton2() {
        String keyMenu;
        do {
            generateButton2Menu();
            System.out.println("Please, select menu point.");
            keyMenu = scanner.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).run();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("B"));
    }

    private void pressButton3() {
        String keyMenu;
        do {
            generateButton3Menu();
            System.out.println("Please, select menu point.");
            keyMenu = scanner.nextLine().toUpperCase();
            try {
                while (keyMenu.equals("")) {
                    keyMenu = scanner.nextLine().toUpperCase();
                }
                methodsMenu.get(keyMenu).run();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("B"));
    }

    private void pressButton4() {
        logger.info("Please, enter some number of text lines: ");
        textInChars = AppMethods.toCharList(
                AppMethods.getTextFromUser());
        textInStrings = AppMethods.toStringList(textInChars);
        String keyMenu;
        do {
            generateButton4Menu();
            System.out.println("Please, select menu point.");
            keyMenu = scanner.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).run();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("B"));
    }

    private void generateButton2Menu() {
        menu.clear();
        menu.add("1 - Set brightness to max");
        menu.add("2 - Set brightness to avg");
        menu.add("3 - Set brightness to min");
        menu.add("4 - Set brightness to zero");
        menu.add("B - Back");

        methodsMenu.clear();
        methodsMenu.put("1", this::setBrightnessToMax);
        methodsMenu.put("2", this::setBrightnessToAvg);
        methodsMenu.put("3", this::setBrightnessToMin);
        methodsMenu.put("4", this::setBrightnessToZero);
        outputMenu();
    }

    private void setBrightnessToMax() {
        Map<Integer, Command> commandMenu = generateCommandMenu();
        commandMenu.get(1).execute();
    }

    private void setBrightnessToMin() {
        Map<Integer, Command> commandMenu = generateCommandMenu();
        commandMenu.get(3).execute();
    }

    private void setBrightnessToAvg() {
        Map<Integer, Command> commandMenu = generateCommandMenu();
        commandMenu.get(2).execute();
    }

    private void setBrightnessToZero() {
        Map<Integer, Command> commandMenu = generateCommandMenu();
        commandMenu.get(4).execute();
    }

    private Map<Integer, Command> generateCommandMenu() {
        TurnMinCommand turnMinCommand = new TurnMinCommand();
        Map<Integer, Command> commandMenu = new HashMap<>(4);
        commandMenu.put(1, new Light()::turnMax);
        commandMenu.put(2, new Command() {
            @Override
            public void execute() {
                logger.info("The light brightness is average");
            }
        });
        commandMenu.put(3, turnMinCommand);
        commandMenu.put(4, () ->
        {
            logger.info("The light brightness is zero");
        });
        return commandMenu;
    }

    private void generateButton3Menu() {
        menu.clear();
        menu.add("1 - Generate integer list");
        menu.add("2 - Iterate integer list");
        menu.add("3 - Show list info");
        menu.add("4 - Show number of values bigger than average");
        menu.add("B - Back");

        methodsMenu.clear();
        methodsMenu.put("1", this::generateList);
        methodsMenu.put("2", this::iterateList);
        methodsMenu.put("3", this::showListInfo);
        methodsMenu.put("4", this::showNumberOfElementsBiggerThanAvg);
        outputMenu();
    }

    private void generateList() {
        System.out.println("Enter random bound = ");
        int bound = scanner.nextInt();
        System.out.println("Enter list size = ");
        int size = scanner.nextInt();
        generatedList =
                IntegerListGenerator.generateIntegerList(bound, size);
        generatedList.forEach((e) -> logger.info("list element - " + e));
    }

    private void iterateList() {
        System.out.println("Enter random bound = ");
        int bound = scanner.nextInt();
        System.out.println("Enter list size = ");
        int size = scanner.nextInt();
        generatedList =
                IntegerListGenerator.iterateIntegerList(bound, size);
        generatedList.forEach((e) -> logger.info("list element - " + e));
    }

    private void showListInfo() {
        if (generatedList.size() == 0) {
            logger.warn("Pls generate list first");
        }
        logger.info(IntegerListMethods.getListInfo(generatedList));
    }

    private void showNumberOfElementsBiggerThanAvg() {
        if (generatedList.size() == 0) {
            logger.warn("Pls generate list first");
        }
        logger.info(IntegerListMethods
                .countBiggerThanAverageBySize(generatedList));
    }

    private void generateButton4Menu() {
        menu.clear();
        menu.add("1 - Show number of unique words");
        menu.add("2 - Show sorted list of unique words");
        menu.add("3 - Word count");
        menu.add("4 - Show number of each symbol except upper case");
        menu.add("B - Back");

        methodsMenu.clear();
        methodsMenu.put("1", this::showNumberOfUniqueWords);
        methodsMenu.put("2", this::showSetOfUniqueWords);
        methodsMenu.put("3", this::showWordCount);
        methodsMenu.put("4", this::showCharacterCount);
        outputMenu();
    }

    private void showNumberOfUniqueWords() {
        logger.info(AppMethods.getNumberOfUniqueWords(textInStrings));
    }

    private void showSetOfUniqueWords() {
        AppMethods.getSortedWords(textInStrings).forEach(logger::info);
    }

    private void showWordCount() {
        Map<String, Integer> wordCount = AppMethods.getWordCount(textInStrings);
        wordCount.forEach((k, v) -> logger.info("Word = " + k
                + ", entries = " + v));
    }

    private void showCharacterCount() {
        Map<Character, Integer> symbolCount =
                AppMethods.getSymbolsCount(textInChars);
        symbolCount.forEach((k, v) -> logger.info("Letter = " + k
                + ", entries = " + v));
    }


}
