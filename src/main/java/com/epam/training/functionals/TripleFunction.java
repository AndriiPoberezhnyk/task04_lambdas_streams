package com.epam.training.functionals;

@FunctionalInterface
public interface TripleFunction<T> {
    T apply(T a, T b, T c);
}
