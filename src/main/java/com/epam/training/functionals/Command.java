package com.epam.training.functionals;

@FunctionalInterface
public interface Command {
    void execute();
}
