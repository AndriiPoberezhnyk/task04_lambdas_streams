package com.epam.training.listGenerators;

import com.epam.training.Main;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class IntegerListGenerator {
    private static final Logger logger =
            LogManager.getLogger(Main.class.getName());

    public static List<Integer> generateIntegerList(int boundVal, int size){
        return Stream
                .generate(()-> 1 + new Random().nextInt(boundVal))
                .limit(size)
                .collect(Collectors.toList());
    }

    public static List<Integer> iterateIntegerList(int boundVal,int size){
        return Stream
                .iterate((int)(Math.random()*boundVal),
                        n -> (int)(Math.random()*boundVal))
                .limit(size)
                .collect(Collectors.toList());
    }

}
